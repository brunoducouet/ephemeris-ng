<?php
include_once('log.php');

class eph extends log {

    private $dbhost='localhost';
    private $charset = 'utf8';
    private $prefix='eph_';
    public  $bithd=TRUE;
    
    protected $connection;
	protected $query;
	public $query_count = 0;
	
	public function __construct() {
        try {
            $dbhost=$this->dbhost;
            @$dbuser=$_SESSION['user'];
            @$dbpasswd=$_SESSION['passwd'];
            $dbname=$this->prefix.$dbuser;
            mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_INDEX);
            $this->connection = new mysqli($this->dbhost, $dbuser, $dbpasswd, $dbname);
            $this->connection->set_charset($this->charset);
            $this->connection->query("SET lc_time_names = 'fr_fr'");
		}
		catch(mysqli_sql_exception $e) {
            $this->new_log("Erreur lors de la connection à la base de données.", 1) ;          
        }
	}
    
    function __destruct() {
        if (isset($this->connection)) { 
            $this->connection->close();
        }
    }
    
    public function logout() {
        $_SESSION = array();
    }
    
    public function minmax() {
        try {
            $res = $this->connection->query("SELECT TIMESTAMPDIFF(second,FROM_UNIXTIME(0),min(date)) min, UNIX_TIMESTAMP(max(date)) max, DATE_FORMAT(min(date), '%d-%m-%Y') min_d, DATE_FORMAT(max(date), '%d-%m-%Y') max_d from items");
            return $res->fetch_assoc();
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
    
    public function liste_cat($type) {
        try {
            if ($type) { $sql_query = "SELECT * FROM category WHERE parent = $type ORDER BY name ASC "; }
            else       { $sql_query = "SELECT * FROM category WHERE parent IS NULL ORDER BY name ASC "; }
            $res = $this->connection->query($sql_query);
            while ($row = $res->fetch_assoc()) {
                echo "<option value='$row[category_id]'>$row[name]</option>\n";
            }
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
        
    public function liste_birthday() {
         try {
             $sql_query = 'SELECT date, DATE_FORMAT(date,"%a %e %M %Y") AS date, name FROM items WHERE (category_id=1) 
                AND ((day(date) >= day(CURDATE()) AND month(date) = month(CURDATE()))
                OR (day(date) < day(CURDATE()) AND month(date) = month(CURDATE())+1 MOD 12)) ORDER BY month(date), day(date) ASC';
             $res=$this->connection->query($sql_query);
             if (!$res->num_rows) { echo "<p>Pas d'anniversaire prochainement</p>"; }
             echo "<ul>";
             while ($row = $res->fetch_assoc()) {
                 echo "<li>$row[date] : $row[name]</li>";
             }
             echo "</ul>";
         }
         catch(mysqli_sql_exception $e) {
             $this->new_log($e->getMessage(), 1) ;
         }
     }
     
     public function liste_next_ev() {
         try {
             $sql_query = 'SELECT date, DATE_FORMAT(date,"%a %e %M %Y") AS datefr, name FROM items WHERE category_id!=1 
                AND (date >= date(now()) AND date <= DATE_ADD(now(), INTERVAL 30 DAY)) ORDER BY date';
             $res=$this->connection->query($sql_query);
             if (!$res->num_rows) { echo "<p>Pas d'événement prochainement</p>"; }
             echo "<ul>";
             while ($row = $res->fetch_assoc()) {
                 echo "<li>$row[datefr] : $row[name]</li>";
             }
             echo "</ul>";
         }
         catch(mysqli_sql_exception $e) {
             $this->new_log($e->getMessage(), 1);
         }
     }
     
     public function new_cat($cat, $sub_cat) {
        $cat = $this->clean($cat);
        $cat = ucfirst(strtolower($cat));
        $sql_query = "INSERT INTO `category` (`name`, `parent`) VALUES ('$cat', $sub_cat)";
        try {
            $res=$this->connection->query($sql_query);
            $this->new_log("Création de la catégorie réussie", 0);
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }    
    }
     
    public function find_by_tag($find_tag,$debut,$fin) {
        date_default_timezone_set('Europe/Paris');
        $debut=date('Y-m-d',$debut);
        $fin=date('Y-m-d',$fin);
        $sql_query = "SELECT category.name as cname, date, DATE_FORMAT(date,\"%a %e %M %Y\") AS date_f, items.name, tags.name as tags 
            FROM items, items_has_tags i, tags, category 
            WHERE items.item_id = i.item_id AND i.tag_id = tags.tag_id and items.category_id = category.category_id 
            AND tags.tag_id = $find_tag
            AND date BETWEEN '$debut' AND '$fin'
            ORDER by date DESC";
        
        try {
            $res=$this->connection->query($sql_query);
            if ($res->num_rows) {
                echo "<fieldset class=res><ul>";
                while ($row = $res->fetch_assoc()) {
                    echo "<li><span class=date>$row[date_f]</span> - Catégorie : $row[cname]";
                    echo "<br/>$row[name]\n";
                }
                echo "</ul></fieldset>";
            }
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
    
    public function find_by_cat($find_cat,$debut,$fin) {
        date_default_timezone_set('Europe/Paris');
        $debut=date('Y-m-d',$debut);
        $fin=date('Y-m-d',$fin);
        $sql_query = "SELECT category.name as cname, date, DATE_FORMAT(date,\"%a %e %M %Y\") AS date_f, items.name,  items.item_id , tags.name as tags
            FROM category, items
            left join items_has_tags i on i.item_id = items.item_id
            left join tags on i.tag_id = tags.tag_id
            WHERE  items.category_id = category.category_id 
            AND (items.category_id=$find_cat 
            OR items.category_id IN (SELECT category_id FROM category WHERE parent = $find_cat))
            AND date BETWEEN '$debut' AND '$fin'
            ORDER by date DESC";
        try {
            $res=$this->connection->query($sql_query);
            if ($res->num_rows) {
                echo "<fieldset class=res><ul>";
                while ($row = $res->fetch_assoc()) {
                    echo "<li><span class=date>$row[date_f]</span>";
                    if (!empty($row['tags'])) {
                        echo "- Étiquettes : $row[tags]";
                    }
                    echo "<br/>$row[name]</li>\n";
                }
                echo "</ul></fieldset>";
            }
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
    
    public function list_all_cat() {
        try {
            $sql_query = "SELECT * FROM category ORDER BY name ASC ";
            $res=$this->connection->query($sql_query);
            while ($row = $res->fetch_assoc()) {
                echo "<option value='$row[category_id]'>$row[name]</option>\n";
            }
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
    
    public function list_all_tag() {
        try {
            $sql_query = "SELECT * FROM tags ORDER BY name ASC ";
            $res=$this->connection->query($sql_query);
            while ($row = $res->fetch_assoc()) {
                echo "<option value='$row[tag_id]'>$row[name]</option>\n";
            }
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
     
    public function new_ev($date, $cat, $sub_cat, $n_desc) { // Ajouter try/catch
        $date         = $this->clean($date);
        $cat          = $this->clean($cat);
        $sub_cat      = $this->clean($sub_cat);
        $n_desc       = $this->clean($n_desc);
        if ($sub_cat != "") { $cat = $sub_cat; }
        $mots = explode(" ", $n_desc);
        $tags_array = preg_grep("/^\*/", $mots);
        $n_desc = str_replace("*", "", "$n_desc");
        $sql_query = "SELECT COUNT(*) as count FROM `items` WHERE `name`= '$n_desc' AND `category_id` = $cat AND `date` = '$date'";
        try { // pour vérifier qu'il n'y aura pas de doublon
            $res=$this->connection->query($sql_query);
            $row = $res->fetch_assoc();
            if ($row['count'] == 0) { // pas de doublon !
                $sql_query = "INSERT INTO `items` (`name`, `category_id`, `date`) VALUES ('$n_desc', $cat, '$date')";
                try { // On ajoute l'événement
                    $this->connection->query($sql_query);
                    $id_item = $this->connection->insert_id;
                    $this->new_log("Création de l'événement réussie", 0);
                    $id_tag_array = array();
                    foreach ($tags_array as $tag) {
                        $tag = strtolower(ltrim($tag, "*"));
                        $sql_query = "SELECT tag_id,name FROM tags WHERE name='$tag'";
                        $res = $this->connection->query($sql_query);
                        $nb_res = $res->num_rows;
                        if ($nb_res) {
                            $row = $res->fetch_array();
                            $id_tag_array['$tag'] = intval($row[0]);
                        }
                        else { 
                            $sql_query = "INSERT INTO `tags` (`name`) VALUES ('$tag')";
                            $this->connection->query($sql_query);
                            $id_tag_array["$tag"] = $this->connection->insert_id;
                            $this->new_log("Création de l'étiquette $tag réussie", 0);
                        }
                    }   
                    foreach ($id_tag_array as $id_tag) {
                        $sql_query = "INSERT INTO `items_has_tags` (`item_id`, `tag_id`) VALUES ($id_item, $id_tag)";
                        if (!$this->connection->query($sql_query)) { $this->new_log($e->getMessage(), 1); }
                    }
                }
                catch(mysqli_sql_exception $e) {
                    $this->new_log($e->getMessage(), 1);
                }
            }
        }
        catch(mysqli_sql_exception $e) {
            $this->new_log($e->getMessage(), 1);
        }
    }
     
    private function clean($data) {
        $data = trim($data);
        $data = nl2br($data);
        $data = $this->connection->real_escape_string($data);
        return $data;
    }
}
