# Ephêmeris

New re writing of ephemeris

Ephêmeris est destiné à gérer et mémoriser les événements passés (et futurs).

## Installation

Ephêmeris nécessite un serveur auto hébergé comprenant :

* un serveur web avec un accès en écriture à l'arborescence.
* MySQL ou MariaDB avec des droits pour créer des bases de données.
 
Vous avez juste à installer le répertoire dans l'arborescence d'un serveur web.

## Utiliser Ephêmeris

A la racine du site, vous pouvez vous identifier (icône de gauche) ou créer un nouvel utilisateur (icône de droite).

* Création d'un utilisateur :

    - Pour créer un utilisateur, vous avez besoin un accès au serveur de bases de données avec des droits de création de nouvelle bases.
    Il est possible que vous ayez besoin de créer préalablement cet accès. (Le serveur web n'ayant généralement pas les droits de s'y connecter avec l'utilisateur root)

    - Pour créer le nouvel utilisateur, entrer le nom et le mot de passe de l'utilisateur ayant les droits d'écriture sur la base ; puis le nom et le mot de passe du nouvel
    utilisateur d'Ephêmeris.
    
* Utilisation :

    Vous pouvez enregistrer des événements dont vous voulez vous souvenir. Ces événements sont classés par catégories, sous catégories (facultatif). Vous pouvez ajouter
    des tags.
    Une catégorie est créée par défaut : Les anniversaires.

    Vous voulez vous souvenir de l'anniversaire de votre mariage le 20 juillet 2010 ; Dans l'onglet Ajout, renter la date du 20 juillet 2010, choisissez la catégorie Anniversaire, Dans Description vous pouvez rentrer le texte "*Mariage à la mairie de xxxxx".

    L'étoile devant Mariage ajoute un tag Mariage.

    Pour le voyage de noce, vous pouvez créer une catégorie voyage, puis créer l’événement "Séjour à xxxxxx du *mariage".

    Les deux événements seront dans deux catégories en ayant le même tag. Vous retrouvez les événements par leur catégorie ou par le tag Mariage.



