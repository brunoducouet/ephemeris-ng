<?php session_start();

$_SESSION['log']="";
include_once('../log.php');
$mylog = new log;

if (!is_writable('../src-sql')) {
    $mylog->aff_log("Le répertoire src-sql doit être accessible en écriture !",1);
}
else {
    echo <<<EOF
    <section id='add-items'>
        <h2>Ajouter un utilisateur</h2>
        <fieldset style="width: 90%; margin: auto;">
            <form method=post class=form>
                <ol>
                <li>
                    <label for=mysql_admin>Admin Mysql</label>
                    <input type=text name=mysql_admin size=15 maxlength=15 id=mysql_admin required="required" placeholder="Nom de l'admin Mysql" />
                </li>
                <li>
                    <label for=mysql_pwd>Mdp Admin Mysql</label>
                    <input type=password name=mysql_pwd size=15 maxlength=15 id=mysql_pwd required="required" placeholder="Mot de passe de l'admin Mysql" />
                </li>
                <li>
                    <label for=eph_name>Nom</label>
                    <input type=text name=eph_name size=15 maxlength=15 id=eph_name required="required" placeholder="Nom du nouvel utilisateur" />
                </li>
                <li>
                    <label for=eph_pwd>Mot de passe</label>
                    <input type=password name=eph_pwd size=5 maxlength=15 id=eph_pwd required="required" placeholder="Mot de passe du nouvel utilisateur" />
                </li>
                </ol>
                <button type=submit id=submit value=Submit>Créer l'utilisateur</button>
            </form>
        </fieldset>
    </section>
EOF;
}


