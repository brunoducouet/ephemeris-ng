<?php session_start();

include_once('../eph.php');
$eph = new eph;

echo <<<EOF
<fieldset class=res>
    <form class=form name=nc method=post>
        <ol>
        <li><label for=new_cat>Nouvelle catégorie</label>
        <input required id=new_cat type=text value="" name=new_cat>
        </li>
        <li>
        <label for=sub_cat>Sous catégorie de :</label>
        <select name=sub_cat id=sub_cat>
            <option value="NULL">* Catégorie parente *</option>
EOF;
$eph->list_all_cat();
echo <<<EOF
         </select>
        </li>
        </ol>
        <svg class="img_tc" onclick="document.forms.nc.submit();" viewBox="0 0 16 16"><use xlink:href="#add_ev"/></svg>
    </form>
</fieldset>

EOF;
