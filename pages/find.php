<?php session_start();

include_once('../eph.php');
$eph = new eph;

$res = $eph->minmax();

echo <<<EOF
<form class='form' name='find_form' method='post'>
<fieldset class=res>
<legend class=legend>Dates de début et de fin</legend>
    <ol>
    <li>
    <output class=w20 name="debut" id="debut">$res[min_d]</output>
    <input class=w20 type="range" name="debut_r" min='$res[min]' max='$res[max]' value='$res[min]'                                                                    
            oninput="this.form.debut.value=convertTimestamp(this.value)">
    <input class=w20 type="range" name="fin_r" min='$res[min]' max='$res[max]' value='$res[max]'                                                                    
        oninput="this.form.fin.value=convertTimestamp(this.value)">
    <output class=w20 name="fin" id="fin">$res[max_d]</output>
    </li>
    </ol>
</fieldset>

<fieldset class=res>
    <legend class=legend>Recherche par catégorie/sous catégorie</legend>
        <ol>
            <li><label for=find_cat>Catégorie</label>
                <select name=find_cat id=find_cat onchange="document.forms.find_form.submit();">
EOF;
$eph->list_all_cat();
echo <<<EOF
                    <option selected>&nbsp;</option>
                </select>
            </li>
        </ol>
</fieldset>
<fieldset class=res>
    <legend class=legend>Recherche par étiquette</legend>
        <ol>
            <li><label for=find_tag>Étiquette</label>
                <select name=find_tag id=find_tag onchange="document.forms.find_form.submit();">
EOF;
$eph->list_all_tag();
echo <<<EOF
                    <option selected>&nbsp;</option>
                </select>
            </li>
            
        </ol>
    </fieldset>
</form>

EOF;
