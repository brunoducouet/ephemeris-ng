// Pour les appels Ajax
function envoieRequete(url,id) {
    var xhr_object = null;
    var position = id;
    if(window.XMLHttpRequest)  xhr_object = new XMLHttpRequest();
    else
        if (window.ActiveXObject)  xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_object.open("GET", url, true);
            xhr_object.onreadystatechange = function() {
                if ( xhr_object.readyState == 4 && xhr_object.status == 200) {
                    document.getElementById(position).innerHTML = xhr_object.responseText;
                }
            }
            xhr_object.send(null);
        }
        
function cleanLogs(id) {    
    var elmnt = document.getElementById(id);

    while (elmnt.hasChildNodes()) {  
        elmnt.removeChild(elmnt.firstChild);
    } 
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
    new ActiveXObject('Microsoft.XMLHTTP') :
    new XMLHttpRequest;
    request.onreadystatechange = function() {
        if (request.readyState == 4 ) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };
    request.open('GET', url, true);
    request.send(null);
}


function doNothing() {}

// Gère les dates

function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		time;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}
	
	// ie: 2013-02-18, 8:35 AM	
	// time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
	time = dd + '-' + mm + '-' + yyyy;
		
	return time;
}


// select liés http://http://siddh.developpez.com/articles/ajax/#LIV-A


function getXhr(){
    var xhr = null; 
    if(window.XMLHttpRequest) // Firefox et autres
        xhr = new XMLHttpRequest(); 
    else if(window.ActiveXObject){ // Internet Explorer 
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    else { // XMLHttpRequest non supporté par le navigateur 
        alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
        xhr = false; 
    } 
    return xhr;
}

/**
* Méthode qui sera appelée sur le click du bouton
*/
function go(){
    var xhr = getXhr();
    // On défini ce qu'on va faire quand on aura la réponse
    xhr.onreadystatechange = function(){
            // On ne fait quelque chose que si on a tout reçu et que le serveur est ok
            if(xhr.readyState == 4 && xhr.status == 200){
                    leselect = xhr.responseText;
                    // On se sert de innerHTML pour rajouter les options a la elmnte
                    document.getElementById('livre').innerHTML = leselect;
            }
    }

    // Ici on va voir comment faire du post
    xhr.open("POST","ajax_select.php",true);
    // ne pas oublier ça pour le post
    xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    // ne pas oublier de poster les arguments
    // ici, l'id de l'auteur
    sel = document.getElementById('auteur');
    idauteur = sel.options[sel.selectedIndex].value;
    xhr.send("idAuteur="+idauteur);
}

function affPage() {
    document.getElementById("page").value="main";
    document.getElementById("quit").submit();
}
    
