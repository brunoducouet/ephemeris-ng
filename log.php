<?php

class log {
    private $log="";
    
    public function new_log($log, $type) {
        if ($type!=0) {
            $new_log = "<span class=red><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#bad'/></svg></span><span class=logs> $log</span><br/>";
        }
        else {
            $new_log = "<span class=green><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#good'/></svg></span><span class=logs> $log</span><br/>";
        }
        $_SESSION['log'] = $_SESSION['log'].$new_log;
    }
    
    public function aff_log($log, $type) {
        $this->new_log($log, $type);
        $this->print_log();
    }
    
    public function print_log() {
        if (isset($_SESSION['log'])) {
            if ($_SESSION['log']!="") {
                echo "<div id=message>".$_SESSION['log']."</div>";
                $_SESSION['log']="";
            }
        }
    }
    
    public function clean_log() {
        $_SESSION['log']!="";
    }
}
